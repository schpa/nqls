defmodule NqlsWeb.Schema do
  @moduledoc false
  use Absinthe.Schema

  # {
  #  items {
  #   id
  #   name
  #  }
  #
  #  entries
  #
  #  item(id:"foo") {
  #   name
  #  }
  # }

  # Example data
  @items %{
    "foo" => %{
      id: "foo",
      name: "Foo"
    },
    "bar" => %{
      id: "bar",
      name: "Bar"
    }
  }

  @desc "An item"
  object :item do
    field :id, :id
    field :name, :string
  end

  object :entry do
    field :id, :id
    field :name, :string
  end


  query do
    field :item, :item do
      arg :id, non_null(:id)
      resolve fn %{id: item_id}, _ ->
        {:ok, @items[item_id]}
      end
    end

    field :items, list_of(:entry) do
      # notice the 2-arity
      resolve fn _, _ ->
        {:ok, [%{id: "x", name: "arbeids"}]}
      end
    end

    field :entries, list_of(:string) do
      resolve fn _, _ ->
        {:ok, ["test", "and", "some", "more 123"]}
      end
    end
  end


end
