defmodule NqlsWeb.Router do
  use NqlsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/"  do
    pipe_through :api

    forward "/graphiql", Absinthe.Plug.GraphiQL,
             schema: NqlsWeb.Schema,
             interface: :simple,
             socket: NqlsWeb.UserSocket,
             context: %{pubsub: NqlsWeb.Endpoint}

  end

#  # Other scopes may use custom stacks.
#   scope "/graphiql", NqlsWeb do
#     pipe_through :api
#
#   end
end
