defmodule NqlsWeb.PageController do
  use NqlsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
