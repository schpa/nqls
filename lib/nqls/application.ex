defmodule Nqls.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application


  def absinthe_subscriptions(name) do
    %{
      type: :supervisor,
      id: Absinthe.Subscription,
      start: {Absinthe.Subscription, :start_link, [name]}
    }
  end

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Nqls.Repo,
      # Start the endpoint when the application starts
      # https://elixirforum.com/t/error-when-adding-absinthe-subscription-to-supervisor-tree/17229
      NqlsWeb.Endpoint,
      absinthe_subscriptions(NqlsWeb.Endpoint),
      # Starts a worker by calling: Nqls.Worker.start_link(arg)
      # {Nqls.Worker, arg},

    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Nqls.Supervisor]
    Supervisor.start_link(children, opts)

  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    NqlsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
