defmodule Nqls.Repo do
  use Ecto.Repo,
    otp_app: :nqls,
    adapter: Ecto.Adapters.Postgres
end
